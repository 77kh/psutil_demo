# 配置websocket 路由
from django.urls import re_path
from chat.chat_consumer import ChatConsumer

websocket_urlpatterns = [
    re_path(r'ws/(?P<websocket>\w+)/$', ChatConsumer.as_asgi())
]
